sysrpl_tools
============

Some tools to make writing SysRPL a little easier inside Emacs.

fs-sysrpl-tools.el
------------------

Provides tools for formatting SysRPL code. 


sysrpl_format.py
----------------

The external code formatter called from **fs-sysrpl-tools**

Initially, indents based off '::' and ';' . Needs work but this is a start.


Inside Emacs, use **load-file** to load **fs-sysrpl-tools.el** and then issue the following
command in the buffer containing SysRPL code.

``` text
M-x fs-sysrpl-format-on-buffer
```

This should format/modify the buffer in place.


See **customize-group fs-sysrpl** for more details.

Cheers ..
