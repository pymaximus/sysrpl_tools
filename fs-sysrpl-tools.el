;;; fs-sysrpl-tools.el  --- Some tools to make SysRPL programming a better experience

;;
;; Copyright (C) 2019 Frank Singleton
;;
;; Author: Frank Singleton <b17flyboy@gmail.com>
;; Version: 0.1.1
;; Keywords: programming, sysrpl
;; URL: https://bitbucket.org/pymaximus/sysrpl_tools
;; Package-Requires: ((emacs "24"))

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;
;; Provides some SysRPL tools that can be useful for programming for HP 50G
;;

;;; Code:

(defgroup fs-sysrpl nil
  "Tools for Sysrpl programming."
  :prefix "fs-sysrpl-"
  :group 'applications)

(defcustom fs-sysrpl-formatter-program "/Users/frank/repos/sysrpl_tools/sysrpl_format.py"
  "External program that formats Sysrpl code."
  :type '(string)
  :group 'fs-sysrpl)

(defun fs-sysrpl-format-on-buffer ()
  "Format the current buffer using Sysrpl formatting."
  (interactive)
  (save-excursion
    (shell-command-on-region (point-min) (point-max) fs-sysrpl-formatter-program t t)
    (whitespace-cleanup)
    ))

(provide 'fs-sysrpl-tools)
;;; fs-sysrpl-tools.el ends here
